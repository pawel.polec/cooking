
<?php
//ini_set('display_startup_errors', 1);
//ini_set('display_errors', 1);
//error_reporting(-1);

function filterArray(array $inputArray, array $columns) {
	$filteredArray = [];
	foreach ($columns as $column) {
		$filteredArray[$column] = $inputArray[$column];
	}
	return $filteredArray;
}

function getTags(array $recipe) {
	$tags = "";
	foreach ($recipe["ingredients"] as $ingr) {
		$tags .= $ingr["name"]." ";
	}
	return $tags;
}

function getBasicList() {
	// Get the contents of the JSON file 
	$strJsonFileContents = file_get_contents("../debug/recepies.json");
	// Convert to array 
	$array = json_decode($strJsonFileContents, true);
	$outArray = [];

	foreach ($array as $recipe) {
		array_push($outArray, filterArray($recipe, ["name", "id"]));

		$outArray[count($outArray)-1]["tags"] = getTags($recipe);
	}

	header('Content-Type: application/json');
	echo json_encode($outArray);
}

function getThumbnailById($id) {
	// Get the contents of the JSON file 
	$strJsonFileContents = file_get_contents("../debug/recepies.json");
	// Convert to array 
	$array = json_decode($strJsonFileContents, true);
	$outArray = [];

	foreach ($array as $recipe) {
		if ($recipe["id"] == $id) {
			array_push($outArray, filterArray($recipe, ["thumbnail", "id"]));
			
			break;
		}
	}

	header('Content-Type: application/json');
	echo json_encode($outArray);
}

function getDetailsById($id) {
	// Get the contents of the JSON file 
	$strJsonFileContents = file_get_contents("../debug/recepies.json");
	// Convert to array 
	$array = json_decode($strJsonFileContents, true);
	$outArray = [];

	foreach ($array as $recipe) {
		if ($recipe["id"] == $id) {
			array_push($outArray, filterArray($recipe, ["image", "id", "type", "description", "ingredients", "name"]));
			
			break;
		}
	}

	header('Content-Type: application/json');
	echo json_encode($outArray);
}

if (isset($_GET['base_lilst']) || empty($_GET)) {
	getBasicList();
} else if (isset($_GET['thumbnail']) && isset($_GET['id'])) {
	getThumbnailById($_GET['id']);
} else if (isset($_GET['details']) && isset($_GET['id'])) {
	getDetailsById($_GET['id']);
}


?>