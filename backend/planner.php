
<?php

//$planerDBFile="../debug/planner.json";

define("planerDBFile", "../debug/planner.json");
define("recepiesDBFile", "../debug/recepies.json");


//ini_set('display_startup_errors', 1);
//ini_set('display_errors', 1);
//error_reporting(-1);


//Syntax:
//?action=add&id=2&date=23.05.2020&portions=7
//?action=get&id=2
//?action=get&s=22.04.2020&e=26.04.2020

function getNameById($id) {
	$array = loadDb(recepiesDBFile);

	foreach ($array as $recipe) {
		if ($recipe["id"] == $id) {
			return $recipe["name"];
		}
	}
}

function getTypeById($id) {
	$array = loadDb(recepiesDBFile);

	foreach ($array as $recipe) {
		if ($recipe["id"] == $id) {
			return $recipe["type"];
		}
	}
}
function filterDbByDate(array $db, $startDate, $endDate) {
	$filteredArray = [];
	foreach ($db as $entry) {
		if (
			(DateTime::createFromFormat('d.m.Y', $entry["date"])>=DateTime::createFromFormat('d.m.Y', $startDate)) &&
			(DateTime::createFromFormat('d.m.Y', $entry["date"])<=DateTime::createFromFormat('d.m.Y', $endDate))
		) {
		$entry["name"] = getNameById($entry["id"]);
		$entry["type"] = getTypeById($entry["id"]);
			array_push($filteredArray, $entry);
		}
	}
	return $filteredArray;
}

function filterDbById(array $db, $id, $from="01.01.2020") {
	$filteredArray = [];
	foreach ($db as $entry) {
		if ($entry["id"]==$id && (DateTime::createFromFormat('d.m.Y', $entry["date"])>=DateTime::createFromFormat('d.m.Y', $from))) {
			array_push( $filteredArray, $entry);
		}
	}
	return $filteredArray;
}

function addEntry(array $db, $id, $date, $portions, &$status) {
	$isNew = true;
	foreach ($db as $i => $entry) {
		if ($entry["id"]==$id && (DateTime::createFromFormat('d.m.Y', $entry["date"])==	DateTime::createFromFormat('d.m.Y', $date)) ) {
			$db[$i]["portions"] = intval($portions);
			$isNew = false;
			$status["status"]="edited";
			$status["load"]=$db[$i];
			
		}
	}
	
	if ($isNew) {
		$newItem = (object) ["id" => $id, "date" => $date, "portions" => intval($portions)];
		array_push($db, $newItem);
		$newItem->name = getNameById($id);
		$newItem->type = getTypeById($id);
		$status["status"]="added";
		$status["load"]=$newItem;
	}
	return $db;
}

function saveDb($db) {
	file_put_contents(planerDBFile, json_encode($db));
}

function loadDb($fileName) {
	// Get the contents of the JSON file 
	$strJsonFileContents = file_get_contents($fileName);
	// Convert to array 
	return json_decode($strJsonFileContents, true);
}

function cleanDb(array $db) {
	$dbChanged = false;
	foreach ($db as $i=>$plan) {
		if (DateTime::createFromFormat('d.m.Y', $plan["date"])<new DateTime()) {
			unset($db[$i]);
			$dbChanged = true;
		}
	}
	if ($dbChanged) {
		saveDb($db);
	}
	return $db;
}

if (isset($_GET['action'])) {
	$db = loadDb(planerDBFile);
	//$db = cleanDb($db);
	switch ($_GET['action']) {
		case "get":
			$filtered = null;
			if (isset($_GET['s']) && isset($_GET['e'])) {
				$filtered = filterDbByDate($db, $_GET['s'], $_GET['e']);
			} else if (isset($_GET['id'])){
				if (isset($_GET['sd'])) {
					$filtered = filterDbById($db, $_GET['id'], $_GET['sd']);
				} else {
					$filtered = filterDbById($db, $_GET['id']);
				}
			}
			
			if ($filtered) {
				header('Content-Type: application/json');
				echo json_encode($filtered);
			} else {
				header('Content-Type: application/json');
				echo json_encode((object)["state"=>(object)["status"=>"error", "load"=>"Wrong parameters"]]);
			}
			break;
		case "add":
			if (isset($_GET['date']) && isset($_GET['id']) && isset($_GET['portions'])) {
				$status;
				$db = addEntry($db, $_GET['id'], $_GET['date'], $_GET['portions'], $status);
				saveDb($db);
				
				header('Content-Type: application/json');
				echo json_encode((object)["state"=>$status]);
			} else {
				header('Content-Type: application/json');
				echo json_encode((object)["status"=>"error", "load"=>"Wrong parameters"]);
			}
			break;
	}
}
?>