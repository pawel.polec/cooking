<?php
define('STDIN',fopen("php://stdin","r"));
ini_set('display_startup_errors', 1);
ini_set('display_errors', 1);
error_reporting(-1);
require 'vendor/google-api-php-client-2.4.0/vendor/autoload.php';

echo "Dupa".__LINE__ ."<br>";
	require_once 'vendor/google-api-php-client-2.4.0/src/Google/Client.php';     
//require_once 'vendor/google-api-php-client-2.4.0/src/Google/Service/Analytics.php';       
session_start();      
$client = new Google_Client();
    $client->setApplicationName("Client_Library_Examples");
	
    $client->setAuthConfig('/home/pi/credentials.json');
    //$client->setDeveloperKey("{devkey}");  
    //$client->setClientId('{clientid}.apps.googleusercontent.com');
    //$client->setClientSecret('{clientsecret}');
    $client->setRedirectUri('https://cooking.ddns.net/auth.php');
    $client->setScopes(array('https://www.googleapis.com/auth/analytics.readonly'));

    //For loging out.
    if (isset($_GET['logout']) && $_GET['logout'] == "1") {
		unset($_SESSION['token']);
    }   

    // Step 2: The user accepted your access now you need to exchange it.
    if (isset($_GET['code'])) {
        $client->authenticate($_GET['code']);  
        $_SESSION['token'] = $client->getAccessToken();
        $redirect = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'];
        header('Location: ' . filter_var($redirect, FILTER_SANITIZE_URL));
    }

    // Step 1:  The user has not authenticated we give them a link to login    
    if (!$client->getAccessToken() && !isset($_SESSION['token'])) {
        $authUrl = $client->createAuthUrl();
        print "<a class='login' href='$authUrl'>Connect Me!</a>";
        }        

    // Step 3: We have access we can now create our service
    if (isset($_SESSION['token'])) {
        print "<a class='logout' href='".$_SERVER['PHP_SELF']."?logout=1'>LogOut</a><br>";
        $client->setAccessToken($_SESSION['token']);
        $service = new Google_Service_Analytics($client);    

        // request user accounts
        $accounts = $service->management_accountSummaries->listManagementAccountSummaries();

       foreach ($accounts->getItems() as $item) {
        echo "Account: ",$item['name'], "  " , $item['id'], "<br /> \n";        
        foreach($item->getWebProperties() as $wp) {
            echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WebProperty: ' ,$wp['name'], "  " , $wp['id'], "<br /> \n";    

            $views = $wp->getProfiles();
            if (!is_null($views)) {
                foreach($wp->getProfiles() as $view) {
                //  echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;View: ' ,$view['name'], "  " , $view['id'], "<br /> \n";    
                }
            }
        }
    } // closes account summaries

    }
 print "<br><br><br>";
 print "Access from google: " . $_SESSION['token']; 
?>
<?php
// function require_auth() {
	// $AUTH_USER = 'admin';
	// $AUTH_PASS = 'admin';
	// header('Cache-Control: no-cache, must-revalidate, max-age=0');
	// $has_supplied_credentials = !(empty($_SERVER['PHP_AUTH_USER']) && empty($_SERVER['PHP_AUTH_PW']));
	// $is_not_authenticated = (
		// !$has_supplied_credentials ||
		// $_SERVER['PHP_AUTH_USER'] != $AUTH_USER ||
		// $_SERVER['PHP_AUTH_PW']   != $AUTH_PASS
	// );
	// if ($is_not_authenticated) {
		// header('HTTP/1.1 401 Authorization Required');
		// header('WWW-Authenticate: Basic realm="Access denied"');
		// exit;
	// }
	
	// echo "Hello " .$_SERVER['PHP_AUTH_USER'];
// }

// require_auth();
?> 