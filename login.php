<?php

	
	session_start();
	function discardSesion() {
			session_destroy();
			header("Location: https://cooking.ddns.net/login.php");
			exit();
	}
	
	if (!isset($_SESSION['init']))
	{
		session_regenerate_id();
		$_SESSION['init'] = true;
		$_SESSION['ip'] = $_SERVER['REMOTE_ADDR'];
	}
	
	
	if($_SESSION['ip'] != $_SERVER['REMOTE_ADDR'])
	{
		header("Location: https://cooking.ddns.net");
		exit();
	}
 
	$users = array(1 =>
		array('login' => 'user1', 'passwd' => sha1('ppp')),
		array('login' => 'user2', 'passwd' => sha1('ddd')),
		array('login' => 'user3', 'passwd' => sha1('fff'))
	);
	
	function isUserLegit($login, $passwd)
	{
		global $users;
		
		$passwd = sha1($passwd);
		
		foreach($users as $id => $dane)
		{
			if($dane['login'] == $login && $dane['passwd'] == $passwd)
			{
				// O, jest ktos taki - zwroc jego ID
				return $id;
			}
		}
		// Jeżeli doszedłeś aż tutaj, to takiego użytkownika nie ma
		return false;
	} // end isUserLegit();
	// Wlasciwy skrypt
	$invalidLogin = false;
	
	if(!isset($_SESSION['user']))
	{
		// Sesja się zaczyna, wiec initemy użytkownika anonimowego
		$_SESSION['user'] = 0;
	}
	if($_SESSION['user'] > 0)
	{
		// Ktos jest zalogowany
		if($_GET['logout'] && $_GET['logout']=='true')
		{
			discardSesion();
		}
		else
		{
			header("Location: https://cooking.ddns.net");
			exit();
		}
	}
	else
	{
		
		// Niezalogowany
		if($_SERVER['REQUEST_METHOD'] == 'POST')
		{
			if(($id = isUserLegit($_POST['username'], $_POST['passwd'])) !== false)
			{
				// Logujemy usera, wpisal poprawne dane
				
				$_SESSION['user'] = $id;
				header("Location: https://cooking.ddns.net");
				exit();
			}
			else
			{
				$invalidLogin = true;
			}		
		}	
	}
	
?>
<!DOCTYPE html>
<html lang="pl">
  <title>Cooking Planer</title>

  <!-- Custom fonts for this theme -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">

  <!-- Theme CSS -->
  <link href="css/freelancer.min.css" rel="stylesheet">
  
  
</head>

<body id="page-top">

	<div class="container">
    <div class="row">
      <div class="col-sm col-md col-lg-6 mx-auto">
        <div class="card card-signin my-5">
          <div class="card-body">
            <?php if ($invalidLogin) echo '<h3 class="card-title text-center text-danger">Niepoprawne dane logowania.</h3>' ?>
            <h1 class="card-title text-center">Wpisz się</h1>
            <form class="form-signin" method="post" action="login.php">
              <div class="form-label-group">
                <input type="text" name="username" id="login" class="form-control" placeholder="Login" required autofocus>
                <label for="username">Login</label>
              </div>

              <div class="form-label-group">
                <input type="password" name="passwd" id="inputPassword" class="form-control" placeholder="Password" required>
                <label for="inputPassword">Password</label>
              </div>

              <div class="custom-control custom-checkbox mb-3">
                <input type="checkbox" class="custom-control-input" id="customCheck1">
                <label class="custom-control-label" for="customCheck1">Remember password</label>
              </div>
              <button class="btn btn-lg btn-primary btn-block text-uppercase" type="submit">Sign in</button>
              <hr class="my-4">
              <button class="btn btn-lg btn-google btn-block text-uppercase" type="submit"><i class="fab fa-google mr-2"></i> Sign in with Google</button>
              <button class="btn btn-lg btn-facebook btn-block text-uppercase" type="submit"><i class="fab fa-facebook-f mr-2"></i> Sign in with Facebook</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- Bootstrap core JavaScript -->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Plugin JavaScript -->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Contact Form JavaScript -->
  <script src="js/jqBootstrapValidation.js"></script>
  <script src="js/contact_me.js"></script>

  <!-- Custom scripts for this template -->

</body>

</html>