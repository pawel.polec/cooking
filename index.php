<?php

  session_start();
  if (!isset($_SESSION['init']) || !isset($_SESSION['user']) || $_SESSION['user'] == 0 )
  {
      header("Location: https://cooking.ddns.net/login.php");
      exit();
  }
?>


<!DOCTYPE html>
<html lang="pl">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="icon" href="favicon.ico?v=2" />

  <title>Cooking Planer</title>

  <!-- Custom fonts for this theme -->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">

  <!-- Theme CSS TODO: USE min.css-->
  <link href="css/freelancer.css" rel="stylesheet">
  <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <!-- TODO List CSS TODO: move to external file and generate *.min.css file -->
  <style>
  
  :root {
  --fade-out: rgba(255,255,255,0.7);
  --fade-in: rgba(255,255,255,0);
  }
    .noselect {
    -webkit-touch-planner: none; /* iOS Safari */
      -webkit-user-select: none; /* Safari */
       -khtml-user-select: none; /* Konqueror HTML */
         -moz-user-select: none; /* Old versions of Firefox */
          -ms-user-select: none; /* Internet Explorer/Edge */
              user-select: none; /* Non-prefixed version, currently
                                    supported by Chrome, Opera and Firefox */
    }
    
    input[type=checkbox] + label {
      text-decoration: none;
    }

    input[type=checkbox]:checked + label {
      text-decoration: line-through;
    }

    .btn-group-xs > .btn, .btn-xs {
      padding: .25rem .4rem;
      font-size: .875rem;
      line-height: .5;
      border-radius: .2rem;
    }
  </style>

  <!-- Calendar notes CSS TODO: move to external file and generate *.min.css file -->
  <style>
  
	.bottom {
		position: absolute;
		bottom: 0;
		left: 0;
		width: 100%;
	}
	.img-dish {
		position: absolute;
		bottom: 0;
		left: 0;
		width: 100%;
		height: 30%;
		background: rgba(0,0,0,0.1);
		background: -moz-linear-gradient(top, rgba(0,0,0,0.1) 0%, rgba(0,0,0,0.86) 100%);
		background: -webkit-gradient(left top, left bottom, color-stop(0%, rgba(0,0,0,0.1)), color-stop(100%, rgba(0,0,0,0.86)));
		background: -webkit-linear-gradient(top, rgba(0,0,0,0.1) 0%, rgba(0,0,0,0.86) 100%);
		background: -o-linear-gradient(top, rgba(0,0,0,0.1) 0%, rgba(0,0,0,0.86) 100%);
		background: -ms-linear-gradient(top, rgba(0,0,0,0.1) 0%, rgba(0,0,0,0.86) 100%);
		background: linear-gradient(to bottom, rgba(0,0,0,0.1) 0%, rgba(0,0,0,0.86) 100%);
		filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#000000', endColorstr='#000000', GradientType=0 );
    }
    .fade-right:after {
      content: "";
      position:absolute;
      right: 0px;
      top: 0px;
      width: 5vw;
      height: 100%;
      background: rgba(0,0,0,0.3);/* Old Browsers */
      background: -moz-linear-gradient(left,                           var(--fade-in) 0%,                var(--fade-out) 100%); /* FF3.6+ */
      background: -webkit-gradient(left top, right top, color-stop(0%, var(--fade-in)), color-stop(100%, var(--fade-out)));/* Chrome, Safari4+ */
      background: -webkit-linear-gradient(left,                        var(--fade-in) 0%,                var(--fade-out) 100%); /* Chrome10+,Safari5.1+ */
      background: -o-linear-gradient(left,                             var(--fade-in) 0%,                var(--fade-out) 100%); /* Opera 11.10+ */
      background: -ms-linear-gradient(left,                            var(--fade-in) 0%,                var(--fade-out) 100%); /* IE 10+ */
      background: linear-gradient(to right,                            var(--fade-in) 0%,                var(--fade-out) 100%);/* W3C */
    }
    .fade-right{position:relative;}
 
    .fade-left:before {
      z-index: 99;
      content: "";
      position: absolute;
      left: 0px;
      top: 0px;
      width: 5vw;
      height: 100%;
      background: rgba(0,0,0,0.3);
      background: -moz-linear-gradient(left,                           var(--fade-out) 0%,                var(--fade-in) 100%);
      background: -webkit-gradient(left top, right top, color-stop(0%, var(--fade-out)), color-stop(100%, var(--fade-in)));
      background: -webkit-linear-gradient(left,                        var(--fade-out) 0%,                var(--fade-in) 100%);
      background: -o-linear-gradient(left,                             var(--fade-out) 0%,                var(--fade-in) 100%);
      background: -ms-linear-gradient(left,                            var(--fade-out) 0%,                var(--fade-in) 100%);
      background: linear-gradient(to right,                            var(--fade-out) 0%,                var(--fade-in) 100%);
    }

    .card.bg-breakfast .card-header, .card.bg-breakfast .card-footer {
      background-color: #ffc325;
      color: black;
    }
    .card.bg-breakfast .card-body {
      background-color: #ffee60;
      color: black;
    }

    .card.bg-diner .card-header, .card.bg-diner .card-footer {
      background-color: #cc2222;
      color: white;
      text-shadow: 1px 1px #000000;
    }
    .card.bg-diner .card-body {
      background-color: #ff8288;
      color: white;
      text-shadow: 1px 1px #000000;
    }
    
	.card.bg-deser .card-header, .card.bg-deser .card-footer {
      background-color: #002222;
      color: white;
      text-shadow: 1px 1px #000000;
    }
    .card.bg-deser .card-body {
      background-color: #008288;
      color: white;
      text-shadow: 1px 1px #000000;
    }
    
    .table.table-planner {
	  skip-border-radius: 25px !important;
	  skip-border-width: 2px !important;
	  skip-border-style: solid !important;
      width: 200ch;
      table-layout: fixed;
    }
	skip.table-planner * {
		border-radius: inherit;
	}
	
	.todo-list {
		color: black;
	}
	
	.todo-container {
		padding-top: 3em;
	}
	
	.todo-remove-abort {
		background-color: var(--danger);
		color: var(--yellow);
		padding: 10px;
		font-weight: bold;
		text-align: right;
		z-index:20;
	}
  </style>
  
  <style>
	.bd-planner {
		padding: 1.25rem;
		margin-top: 1.25rem;
		margin-bottom: 1.25rem;
		border: 1px solid #eee;
		border-left-width: .25rem;
		border-radius: .5rem
	}

	.bd-planner h4 {
		margin-top: 0;
		margin-bottom: .25rem;
	}

	.bd-planner p:last-child {
		margin-bottom: 0
	}

	.bd-planner code {
		border-radius: .25rem
	}

	.bd-planner+.bd-planner {
		margin-top: -.25rem
	}

	.bd-planner-dessert {
		border-left-color: #5bc0de
	}

	.bd-planner-dessert h4:before {
		content: "Deser";
	}

	.bd-planner-dessert h4 {
		color: #5bc0de;
	}

	.bd-planner-other {
		border-left-color: #fd4f92
	}

	.bd-planner-other h4:before {
		content: "Różności";
	}

	.bd-planner-other h4 {
		color: #fd4f92;
	}

	.bd-planner-breakfast {
		border-left-color: #f0ad4e
	}

	.bd-planner-breakfast h4:before {
		content: "Śniadanie";
	}

	.bd-planner-breakfast h4 {
		color: #f0ad4e
	}

	.bd-planner-dinner {
		border-left-color: #d9534f
	}

	.bd-planner-dinner h4:before {
		content: "Obiad";
	}

	.bd-planner-dinner h4 {
		color: #d9534f
	}
	
	.bd-planner-new {
		border-left-color: var(--success);
	}

	.bd-planner-new h4 {
		color: var(--success);
		text-align: center;
	}
  </style>
  
  <!--datapicker-->
  <style>
	.ui-state-default {
		border: 2px solid var(--secondary) !important;
		border-radius: 4px !important;
	}
	
	.ui-state-highlight {
		border: 2px solid var(--primary) !important;
		background-color: var(--white) !important;
		color: var(--primary) !important
	}
	
	.ui-state-active {
		background-color: var(--secondary) !important;
		color: var(--white) !important
	}
	
	.ui-datepicker-holiday .ui-state-default {
		border: 2px solid var(--danger) !important;
		color: var(--danger) !important
	}
	.ui-datepicker-holiday .ui-state-active {
		background-color: var(--danger) !important;
		color: var(--white) !important
	}
	
	.callendar-notes {max-height:3rem}
  </style>
  
  <!-- Recipes -->
  <style>
	.aspect-ratio-keeper {
		margin-top:71%;
	}
	
	.item-with-a-ratio {
		position:absolute;
		top:0;
		left:0;
		right:0;
		bottom:0
	}
  </style>
</head>

<body id="page-top">

  <!-- Navigation -->
  <nav class="navbar navbar-expand-lg bg-secondary text-uppercase fixed-top" id="mainNav">
    <div class="container">
      <a class="navbar-brand js-scroll-trigger" href="#page-top">Cooking planner</a>
      <button class="navbar-toggler navbar-toggler-right text-uppercase font-weight-bold bg-primary text-white rounded" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        Menu
        <i class="fas fa-bars"></i>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item mx-0 mx-lg-1">
            <a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="#recipes">Przepisy</a>
          </li>
          <li class="nav-item mx-0 mx-lg-1">
            <a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="#calendar">Planer</a>
          </li>
          <li class="nav-item mx-0 mx-lg-1">
            <a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="#cart">Lista</a>
          </li>
          <li class="nav-item mx-0 mx-lg-1">
            <a class="nav-link py-3 px-0 px-lg-3 rounded" href="login.php?logout=true">Wyloguj</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>

  <!-- Masthead -->
  <header class="masthead bg-primary text-white text-center">
    <div class="container d-flex align-items-center flex-column">

      <!-- Masthead Avatar Image -->
      <img class="masthead-avatar mb-5" src="img/avataaars_f.svg" alt="">

      <!-- Masthead Heading -->
      <h1 class="masthead-heading text-uppercase mb-0">Cooking Planner</h1>

      <!-- Icon Divider -->
      <div class="divider-custom divider-light">
        <div class="divider-custom-line"></div>
        <div class="divider-custom-icon">
          <i class="fas fa-star"></i>
        </div>
        <div class="divider-custom-line"></div>
      </div>

      <!-- Masthead Subheading -->
      <p class="masthead-subheading font-weight-light mb-0">Niezastąpiony pomocnik kuchenny. Zapisuj przepisy, planuj obiady na cały tydzień, generuj listy zakupów.</p>

    </div>
  </header>



  <!-- Calendar Section -->
  <section class="page-section" id="calendar">
    <div class="container">

      <!-- Calendar Section Heading -->
      <h2 class="page-section-heading text-center text-uppercase">Planer</h2>

      <!-- Icon Divider -->
      <div class="divider-custom">
        <div class="divider-custom-line"></div>
        <div class="divider-custom-icon">
          <i class="fas fa-star"></i>
        </div>
        <div class="divider-custom-line"></div>
      </div>

      <!-- Calendar Section Content -->
      <div class="row fade-right fade-left" id="plannerMain">
        <div class="table-responsive bg-white">
			<!-- TODO: use separate divs display:inline-block bootstrap tables are almost imposible to get corners curved
			react to swipe: https://api.jquerymobile.com/swipe/
			react to mouse wheel (need left-right) http://jsfiddle.net/BXhzD/
			-->
          <table class="table table-secondary table-bordered table-planner">
            <thead class="thead-dark">
              <tr class="">
                <th class="w-25">Poniedziałek (<span class="planner_mo_date">-</span>) <a class="btn btn-outline-light float-right btn-xs js-scroll-trigger" href="#recipes" title="Dodaj do listy"><i class="fas fa-plus fa-xs"></i></a></th>
                <th class="w-25">Wtorek (<span class="planner_tu_date">-</span>)       <a class="btn btn-outline-light float-right btn-xs js-scroll-trigger" href="#recipes" title="Dodaj do listy"><i class="fas fa-plus fa-xs"></i></a></th>
                <th class="w-25">Środa (<span class="planner_we_date">-</span>)        <a class="btn btn-outline-light float-right btn-xs js-scroll-trigger" href="#recipes" title="Dodaj do listy"><i class="fas fa-plus fa-xs"></i></a></th>
                <th class="w-25">Czwartek (<span class="planner_th_date">-</span>)     <a class="btn btn-outline-light float-right btn-xs js-scroll-trigger" href="#recipes" title="Dodaj do listy"><i class="fas fa-plus fa-xs"></i></a></th>
                <th class="w-25">Piątek (<span class="planner_fr_date">-</span>)       <a class="btn btn-outline-light float-right btn-xs js-scroll-trigger" href="#recipes" title="Dodaj do listy"><i class="fas fa-plus fa-xs"></i></a></th>
                <th class="w-25">Sobota (<span class="planner_sa_date">-</span>)       <a class="btn btn-outline-light float-right btn-xs js-scroll-trigger" href="#recipes" title="Dodaj do listy"><i class="fas fa-plus fa-xs"></i></a></th>
                <th class="w-25">Niedziela (<span class="planner_su_date">-</span>)    <a class="btn btn-outline-light float-right btn-xs js-scroll-trigger" href="#recipes" title="Dodaj do listy"><i class="fas fa-plus fa-xs"></i></a></th>
              </tr>
            </thead>
            <tbody>
              <tr class="">
                <td>
				  <div class="planner_mo"></div>
				  <a class="js-scroll-trigger" href="#recipes">
				    <div class="bd-planner bd-planner-new bg-light p-0">
				      <h4><button class="btn btn-outline-success btn-xs" title="Dodaj do listy"><i class="fas fa-plus fa-xs"></i></button></h4>
				    </div>
				  </a>
                </td>
                <td>
				  <div class="planner_tu"></div>
				  <a class="js-scroll-trigger" href="#recipes">
				    <div class="bd-planner bd-planner-new bg-light p-0">
				      <h4><button class="btn btn-outline-success btn-xs" title="Dodaj do listy"><i class="fas fa-plus fa-xs"></i></button></h4>
				    </div>
				  </a>
                </td>
                <td>
				  <div class="planner_we"></div>
				  <a class="js-scroll-trigger" href="#recipes">
				    <div class="bd-planner bd-planner-new bg-light p-0">
				      <h4><button class="btn btn-outline-success btn-xs" title="Dodaj do listy"><i class="fas fa-plus fa-xs"></i></button></h4>
				    </div>
				  </a>
                </td>
                <td>
				  <div class="planner_th"></div>
				  <a class="js-scroll-trigger" href="#recipes">
				    <div class="bd-planner bd-planner-new bg-light p-0">
				      <h4><button class="btn btn-outline-success btn-xs" title="Dodaj do listy"><i class="fas fa-plus fa-xs"></i></button></h4>
				    </div>
				  </a>
                </td>
                <td>
				  <div class="planner_fr"></div>
				  <a class="js-scroll-trigger" href="#recipes">
				    <div class="bd-planner bd-planner-new bg-light p-0">
				      <h4><button class="btn btn-outline-success btn-xs" title="Dodaj do listy"><i class="fas fa-plus fa-xs"></i></button></h4>
				    </div>
				  </a>
                </td>
                <td>
				  <div class="planner_sa"></div>
				  <a class="js-scroll-trigger" href="#recipes">
				    <div class="bd-planner bd-planner-new bg-light p-0">
				      <h4><button class="btn btn-outline-success btn-xs" title="Dodaj do listy"><i class="fas fa-plus fa-xs"></i></button></h4>
				    </div>
				  </a>
                </td>
                <td>
                  <div class="planner_su"></div>
				  <a class="js-scroll-trigger" href="#recipes">
				    <div class="bd-planner bd-planner-new bg-light p-0">
				      <h4><button class="btn btn-outline-success btn-xs" title="Dodaj do listy"><i class="fas fa-plus fa-xs"></i></button></h4>
				    </div>
				  </a>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </section>


  <!-- Cart Section -->
  <section class="page-section bg-primary text-white mb-0 text-white" id="cart">
    <div class="container" id="listsContainer">

      <!-- Cart Section Heading -->
      <h2 class="page-section-heading text-center text-uppercase mb-0 text-white">Lista zakupów</h2>

      <!-- Icon Divider -->
      <div class="divider-custom divider-light">
        <div class="divider-custom-line"></div>
        <div class="divider-custom-icon">
          <button class="btn btn-outline-light btn-md" data-toggle="modal" data-target="#todoListAddListModal" title="Dodaj listę"><i class="fas fa-plus fa-ms"></i></button>
        </div>
        <div class="divider-custom-line"></div>
      </div>

      <!-- Cart Section Form:hede should be list append -->


    </div>
  </section>


  <!-- Recepies Section -->
  <section class="page-section portfolio pt-0" id="recipes">
    <div class="container" id="recipesList">
      <div class="container sticky-top bg-white pb-1" style="padding-top:6rem">
        <!-- Portfolio Section Heading -->
        <h2 class="page-section-heading text-center text-uppercase text-secondary mb-0">Przepisy</h2>
	  
        <div class="input-group">
          <input class="form-control" id="recipesFilterInput" type="text" placeholder="Szukaj..">
          <div class="input-group-append">
			<button class="btn btn-outline-secondary" data-toggle="collapse" data-target="#filterPanel" type="button"><i class="fas fa-filter"></i></button>
            <button class="btn btn-outline-secondary" onclick="$('#recipesFilterInput').val('');$('#recipesFilterInput').trigger('input');"><i class="fas fa-times"></i></button>
          </div>
        </div>
		<div id="filterPanel" class="collapse">
          <button class="btn btn-outline-secondary m-2" data-toggle="false"><i class="fas fa-user-secret mr-2" data-to="fa-globe-africa"></i>Prywatne</button>
        </div>
      </div>
      <!-- Icon Divider -->
      <div class="divider-custom">
        <div class="divider-custom-line"></div>
        <div class="divider-custom-icon">
          <i class="fas fa-star"></i>
        </div>
        <div class="divider-custom-line"></div>
      </div>

      <!-- Portfolio Grid Items -->

    </div>
  </section>

  <!-- Footer -->
  <footer class="footer text-center">
    <div class="container">
      <div class="row">

        <!-- Footer Location -->
        <div class="col-lg-4 mb-5 mb-lg-0">
          <h4 class="text-uppercase mb-4">Autors</h4>
          <p class="lead mb-0">Piotr Grzelczak
          <p class="lead mb-0">Marek Mróz
          <p class="lead mb-0">Paweł Połeć
            <!--<br>void.pawel.polec@gmail.com</p>-->
        </div>

        <!-- Footer Social Icons -->
        <div class="col-lg-4 mb-5 mb-lg-0">
          <h4 class="text-uppercase mb-4">O aplikacji</h4>
          <a class="btn btn-outline-light btn-social mx-1" href="#">
            <i class="fab fa-fw fa-facebook-f"></i>
          </a>
          <a class="btn btn-outline-light btn-social mx-1" href="#">
            <i class="fab fa-fw fa-twitter"></i>
          </a>
          <a class="btn btn-outline-light btn-social mx-1" href="#">
            <i class="fab fa-fw fa-linkedin-in"></i>
          </a>
          <a class="btn btn-outline-light btn-social mx-1" href="#">
            <i class="fab fa-fw fa-dribbble"></i>
          </a>
        </div>

        <!-- Footer About Text -->
        <div class="col-lg-4">
          <h4 class="text-uppercase mb-4">Użyte w projekcie:</h4>
          <p class="lead mb-0">Freelance theme by <a href="https://startbootstrap.com/" target="_blank">Start Bootstrap</a>.</p>
          <p class="lead mb-0">jQuery by <a href="https://jquery.com/" target="_blank">The jQuery Foundation</a>.</p>
          <p class="lead mb-0">Bootstrap by the <a href="https://getbootstrap.com/" target="_blank">Bootstrap team</a>.</p>
        </div>

      </div>
    </div>
  </footer>

  <!-- Copyright Section -->
  <section class="copyright py-4 text-center text-white">
    <div class="container">
      <small>Copyright &copy; cooking.ddns.net 2020</small>
    </div>
  </section>

  <!-- Scroll to Top Button (Only visible on small and extra-small screen sizes) -->
  <div class="scroll-to-top d-lg-none position-fixed ">
    <a class="js-scroll-trigger d-block text-center text-white rounded" href="#page-top">
      <i class="fa fa-chevron-up"></i>
    </a>
  </div>

  <!-- Portfolio Modals -->

  <!-- Portfolio Modal 1 -->
  <div class="portfolio-modal modal fade" id="recipeDetailsModal" tabindex="-1" role="dialog" aria-labelledby="recipeDetailsModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
      <div class="modal-content">
		  <div class="toast hide" id="recipes-toast-added"  style="z-index:20; position: absolute; top: 3rem; right: 3rem;">
			<div class="toast-header">
			</div>
			<div class="toast-body">
		    </div>
		  </div>
		<div class="text-right pt-4 pr-2">
		  <button type="button" class="close position-static px-3" data-dismiss="modal" aria-label="Zamknij">
            <span aria-hidden="true">
              <i class="fas fa-times"></i>
            </span>
          </button>
		  <button type="button" class="close position-static px-3" id="data-picker-btn">
            <span aria-hidden="true">
              <i class="fa fa-calendar"></i>
            </span>
          </button>

		  <button type="button" class="close position-static px-3" data-dismiss="modal" aria-label="Zamknij" data-toggle="modal" data-target="#addNewRecipe">
            <span aria-hidden="true">
              <i class="fas fa-edit"></i>
            </span>
          </button>
		</div>
        <div class="modal-body text-center pt-0">
          <div class="container">
            <div class="row justify-content-center">
              <div class="col-lg-8">
                <!-- Portfolio Modal - Title -->
                <h2 class="portfolio-modal-title text-secondary text-uppercase mb-0">...</h2>
                <!-- Icon Divider -->
                <div class="divider-custom">
                  <div class="divider-custom-line"></div>
                  <div class="divider-custom-icon">
                    <i class="fas fa-star"></i>
                  </div>
                  <div class="divider-custom-line"></div>
                </div>
                <!-- Portfolio Modal - Image -->
                <img class="portfolio-img img-fluid rounded mb-5" src="img/portfolio/cake.png" alt="">
                <!-- Portfolio Modal - Text -->
                <div class="mb-5 text-justify">
					<div class=" float-left mr-3 p-2 mb-3 border border-primary rounded">
						<h5 class="px-2 pt-2 mb-0">Lista składników:</h5>
						<p class="pb-2 px-2">Liczba porcji: 4</p>
						<ul class="portfolio-ingr-list list-group-flush pl-0 pb-0 mb-0">
							<li class="list-group-item p-1"></li>
						</ul>
					</div>
					<div class="mb-5 text-justify">
					<h5>Opis:</h5>
					<p class="portfolio-descr">... </p>
					</div>
				</div>
                <button class="btn btn-primary" href="#" data-dismiss="modal">
                  <i class="fas fa-times fa-fw"></i>
                  Zamknij okno
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- Portfolio Modal Add Item -->
  <div class="portfolio-modal modal fade" id="todoListAddEditItemModal" tabindex="-1" role="dialog" aria-labelledby="todoListAddEditItemModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
      <div class="modal-content">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">
            <i class="fas fa-times"></i>
          </span>
        </button>
        <div class="modal-body text-center">
          <div class="container">
            <div class="row justify-content-center">
              <div class="col-lg-8">
                <!-- Portfolio Modal - Title -->
                <h2 class="portfolio-modal-title text-secondary text-uppercase mb-0">Dodaj do listy:</h2>
                <!-- Icon Divider -->
                <div class="divider-custom">
                  <div class="divider-custom-line"></div>
                  <div class="divider-custom-icon">
                    <i class="fas fa-star"></i>
                  </div>
                  <div class="divider-custom-line"></div>
                </div>
                <!-- Portfolio Modal - Text -->
				<form id="NewEditItemForm" action="javascript(0);">
                  <div class="form-group">
				    <label for="recipient-name" class="col-form-label">Pozycja:</label>
				    <input type="text" class="form-control" id="recipient-name">
				  </div>
				  <button type="submit" id="NewEditItemSubmit" class="btn btn-primary" href="#" data-dismiss="modal">
                    <i class="fas fa-plus fa-fw"></i>
                    Zapisz
                  </button>
                  <button class="btn btn-primary" href="#" data-dismiss="modal">
                    <i class="fas fa-times fa-fw"></i>
                    Anuluj
                  </button>
				</form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- List remove Modal -->
  <div class="portfolio-modal modal fade" id="todoDeleteListModal" tabindex="-1" role="dialog" aria-labelledby="todoDeleteListModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
      <div class="modal-content">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">
            <i class="fas fa-times"></i>
          </span>
        </button>
        <div class="modal-body text-center">
          <div class="container">
            <div class="row justify-content-center">
              <div class="col-lg-8">
                <!-- Portfolio Modal - Title -->
                <h2 class="portfolio-modal-title text-secondary text-uppercase mb-0">Usuń listę</h2>
                <!-- Icon Divider -->
                <div class="divider-custom">
                  <div class="divider-custom-line"></div>
                  <div class="divider-custom-icon">
                    <i class="fas fa-star"></i>
                  </div>
                  <div class="divider-custom-line"></div>
                </div>
                <!-- Portfolio Modal - Text -->
                <p class="mb-5" id="DeleteListText">Usuwasz listę: [id]</p>
				<button class="btn btn-primary" href="#" id="DeleteListBtnConfirm" data-dismiss="modal">
                  <i class="fas fa-trash fa-fw"></i>
                  Usuń
                </button>
                <button class="btn btn-primary" href="#" data-dismiss="modal">
                  <i class="fas fa-times fa-fw"></i>
                  Anuluj
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- Add List Modal -->
  <div class="portfolio-modal modal fade" id="todoListAddListModal" tabindex="-1" role="dialog" aria-labelledby="todoListAddListModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
      <div class="modal-content">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">
            <i class="fas fa-times"></i>
          </span>
        </button>
        <div class="modal-body text-center">
          <div class="container">
            <div class="row justify-content-center">
              <div class="col-lg-8">
                <!-- Portfolio Modal - Title -->
                <h2 class="portfolio-modal-title text-secondary text-uppercase mb-0">Utwórz listę</h2>
                <!-- Icon Divider -->
                <div class="divider-custom">
                  <div class="divider-custom-line"></div>
                  <div class="divider-custom-icon">
                    <i class="fas fa-star"></i>
                  </div>
                  <div class="divider-custom-line"></div>
                </div>
				
                <!-- Portfolio Modal - Text -->
                <p>Wybierz przedział czasowy, z którego chciał byś wygenerować listę:</p>
				<div class="input-group mb-5">
				  <div class="input-group-prepend">
                    <span class="input-group-text">Od </span>
				  </div>
                  <input class="form-control" type="text" placeholder="Data 1">
				  <div class="input-group-prepend input-group-append">
                    <span class="input-group-text"> do </span>
				  </div>
                  <input class="form-control" type="text" placeholder="Data 2">
				</div>
                <button class="btn btn-primary" id="AddEmptyListBtn" href="#" data-dismiss="modal">
                  <i class="far fa-plus-square"></i>
                  Utwórz pustą
                </button>
                <button class="btn btn-primary" id="AddFilledListBtn" href="#" data-dismiss="modal">
                  <i class="fas fa-plus-square"></i>
                  Utwórz
                </button>
                <button class="btn btn-primary" href="#" data-dismiss="modal">
                  <i class="fas fa-times fa-fw"></i>
                  Anuluj
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- Portfolio Modal 5 -->
  <div class="portfolio-modal modal fade" id="addRecipesToCalendar" tabindex="-1" role="dialog" aria-labelledby="addRecipesToCalendarLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
      <div class="modal-content">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">
            <i class="fas fa-times"></i>
          </span>
        </button>
        <div class="modal-body text-center">
          <div class="container">
            <div class="row justify-content-center">
              <div class="col-lg-8">
                <!-- Portfolio Modal - Title -->
                <h2 class="portfolio-modal-title text-secondary text-uppercase mb-0">Dodaj przepis do planera</h2>
                <!-- Icon Divider -->
                <div class="divider-custom">
                  <div class="divider-custom-line"></div>
                  <div class="divider-custom-icon">
                    <i class="fas fa-star"></i>
                  </div>
                  <div class="divider-custom-line"></div>
                </div>
                <!-- Modal - Text -->
                <p class="mb-5">Dodaj przepis do daty 23.05 <input type="text" id="planner-add-datepicker" style="display: none;"></p>
				<div class="input-group">
				  <input class="form-control" id="planerFilterInput" type="text" placeholder="Szukaj..">
				  <div class="input-group-append">
				    <button class="btn btn-outline-secondary" onclick="$('#planerFilterInput').val('');$('#planerFilterInput').trigger('input');"><i class="fas fa-times"></i></button>
				  </div>
				</div>
                <button class="btn btn-primary" href="#" data-dismiss="modal">
                  <i class="fas fa-times fa-fw"></i>
                  OK
                </button>
                <button class="btn btn-primary" href="#" data-dismiss="modal">
                  <i class="fas fa-times fa-fw"></i>
                  Anuluj
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- New/Edit Recepie -->
  <div class="portfolio-modal modal fade" id="addNewRecipe" tabindex="-1" role="dialog" aria-labelledby="addNewRecipeLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
      <div class="modal-content">
		<div class="text-right pt-4 pr-2">
		  <button type="button" class="close position-static px-3" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">
              <i class="fas fa-times"></i>
            </span>
          </button>
		</div>
        <div class="modal-body text-center pt-0">
          <div class="container">
            <div class="row justify-content-center">
              <div class="col-lg-8">
                <!-- Portfolio Modal - Title -->
                <h2 class="portfolio-modal-title text-secondary text-uppercase mb-0">
					<span class="align-middle">Tytuł</span>
					<button class="btn btn-outline-secondary btn-sm mt-0 align-middle" style="height:100%">
						<i class="fas fa-edit align-middle"></i>
					</button>
				</h2>
                <!-- Icon Divider -->
                <div class="divider-custom">
                  <div class="divider-custom-line"></div>
                  <div class="divider-custom-icon">
                    <i class="fas fa-star"></i>
                  </div>
                  <div class="divider-custom-line"></div>
                </div>
                <!-- Portfolio Modal - Image -->

				<div class="image-upload">
				  <label for="file-input">
					<img class="img-fluid rounded mb-5" src="img/recipes/new-img.png" alt="">
				  </label>

				  <input id="file-input" style="display: none;" type="file" accept="image/*">
				</div>
                <!-- Portfolio Modal - Text -->
                <div class="mb-5 text-justify">
					<div class=" float-left mr-3 p-2 mb-3 border border-primary rounded">
						<h5 class="p-2">Lista składników:</h5>
						<ul class="list-group-flush pl-0 pb-0 mb-0">
							<!--<li class="list-group-item p-1">250g piersi z kurczaka <button class="btn btn-outline-secondary btn-xs float-right"><i class="fas fa-minus"></i></button></li>-->
							<li class="list-group-item p-1 text-center w-100"><button class="btn btn-outline-secondary btn-sm"><i class="fas fa-plus"></i></button></li>
						</ul>
					</div>
					<div class="mb-5 text-justify">
					<h5>Opis:</h5>
					<p><button class="btn btn-outline-secondary btn-sm"><i class="fas fa-edit"></i></button></p>
					</div>
				</div>
                <button class="btn btn-primary" href="#" data-dismiss="modal">
                  <i class="fas fa-check fa-fw"></i>
                  Zapisz
                </button>
                <button class="btn btn-primary" href="#" data-dismiss="modal">
                  <i class="fas fa-times fa-fw"></i>
                  Anuluj
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- Bootstrap core JavaScript -->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  

  <!-- Plugin JavaScript -->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>


  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
	
  <!-- Contact Form JavaScript -->
  <script src="js/jqBootstrapValidation.js"></script>
  <script src="js/contact_me.js"></script>

  <!-- Custom scripts for this template -->
  <script src="js/freelancer.min.js"></script>
  
  <!-- moment.js -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment-with-locales.min.js"></script>
  <script src="vendor/moment/plugins/easter/moment.easter.js"></script>

  <script>
  $(document).ready(function(){
    $("#recipesFilterInput").on("input change", function() {
      var value = $(this).val().toLowerCase();
      $("#recipes .recipe-item").filter(function() {
          all_words = value.split(" ");
          filterVal = true;
          innerText = $(this).text().toLowerCase();
          all_words.forEach(function(entry) {
              if (innerText.indexOf(entry) == -1) {
                  filterVal = false;
              }
          });
        $(this).toggle(filterVal)
      });
    });
  });
  </script>


  
  <script src="js/todo.js"></script>
  <script src="js/recipes.js"></script>
  <script src="js/planner.js"></script>
  <script src="js/cooking.js"></script>

  <script>
  $(window).on('activate.bs.scrollspy', function (e, t) {
    //console.dir(e);
    //console.dir(t);
  })
  </script>
  
  <script>
	var lastPos = 0;
	$(document).ready(function() {
	  $(".table-responsive").scroll(function() {
		  $("#dbg-div").html($(document).scrollLeft());
	  });
	});
  </script>

  <!-- Callendar script -->
  <script>
	let holidays = [//[rule, name, options]
		{type:"date",             name:"Nowy rok",                               options:{date:"01.01"}},
		{type:"date",             name:"Trzech króli",                           options:{date:"06.01"}},
		{type:"date",             name:"Święto pracy",                           options:{date:"01.05"}},
		{type:"date",             name:"Dzień Konstytucji",                      options:{date:"03.05"}},
		{type:"date",             name:"Wniebowzięcie Najświętszej Maryi Panny", options:{date:"15.08"}},
		{type:"date",             name:"Wszystkich Świętych",                    options:{date:"01.11"}},
		{type:"date",             name:"Dzień Niepodległości",                   options:{date:"11.11"}},
		{type:"date",             name:"Pierwszy dzień Bożego Narodzenia",       options:{date:"25.12"}},
		{type:"date",             name:"Drugi dzień Bożego Narodzenia",          options:{date:"26.12"}},
		{type:"easter-dependent", name:"Zielone świątki",                        options:{days_offset:49}},
		{type:"easter-dependent", name:"Pierwszy dzień Wielkiej Nocy",           options:{days_offset:0}},
		{type:"day-of-week",      name:"Niedziela",                              options:{day:0}},
		{type:"easter-dependent", name:"Drugi dzień Wielkiej Nocy",              options:{days_offset:1}},
		{type:"easter-dependent", name:"Boże Ciało",                             options:{days_offset:60}}
	]
	
	$(document).ready(function() {
		
		$('#data-picker-btn').popover({
			title: "Wybierz datę",
			content: 
			"<div>" +
			"<div id=\"recipes-datepicker\">" +
			"</div>" +
			"<div>" +
				"Porcje: <a class=\"btn btn-outline-primary btn-xs px-1 mx-1\" id=\"portion-sub-btn\">-</a><span class=\"align-middle\" id=\"portion-qty\">2</span><a id=\"portion-add-btn\" class=\"btn btn-outline-primary btn-xs px-1 mx-1\">+</a>"+
				"<a class=\"btn btn-outline-primary btn-xs px-1 mr-1 ml-3 accept-po\">Dodaj </a>"+
				'<a class="btn btn-outline-primary btn-xs px-1 mx-1 close-po">Anuluj</a></div>'+
			"</div>"+
			"<div id='planned-list'></div>", 
			html: true, 
			placement: "auto"
		});
		
		$(document).on("click", ".popover #portion-add-btn" , function(){
			$("#portion-qty").html((parseInt($("#portion-qty").html())+1));
		});
		
		$(document).on("click", ".popover #portion-sub-btn" , function(){
			let qty=parseInt($("#portion-qty").html());
			if (qty>1) {
				$("#portion-qty").html(qty-1);
			} else {
				$("#portion-qty").html("1");
			}
		});
		
		$(document).on("click", ".popover .close-po" , function(){
			$(this).parents(".popover").popover('hide');
		});
		
		$(document).on("click", ".popover .accept-po" , function(){
			
			plannerObj.addToPlanner($('#recipeDetailsModal').attr("data-recipe-id"), moment($( "#recipes-datepicker" ).datepicker( "getDate" )).format("DD.MM.YYYY"), parseInt($("#portion-qty").html()), function (jsonResponse) {
				console.dir(jsonResponse);

				// plannerObj.sendIdFilteredListRequest($('#recipeDetailsModal').attr("data-recipe-id"), function(jsonData) {
					// let output = "";
					// if (jsonData && jsonData.length) {
						// output+="<b>Zaplanowałeś tą potrawę na:</b><div class=\"overflow-auto callendar-notes\">"
						// for (let i in jsonData) {
							// output+=jsonData[i]["date"]+"<br>";
						// }
						// output+="</div>";
					// } else {
						// output+="<b>Nie masz jeszcze w planaie tej potrawy</b>";
					// }
					// $('#recipeDetailsModal').data('plannedList', output);
					// $('#planned-list').html(output);
				// });
			});			
			$(this).parents(".popover").popover('hide');
		});
		
		$('#planner-add-datepicker').datepicker({
			dateFormat: "dd/mm/yy", 
			showOn: "both"
		});
		
	});
	$('#data-picker-btn').on("inserted.bs.popover", function () {
		$('#recipes-datepicker').datepicker({
			dateFormat: "dd/mm/yy", 
			minDate: new Date(),
			monthNames: [ "Styczeń", "Luty", "Marzec", "Kwiecień", "Maj", "Czerwiec", "Lipiec", "Sierpień", "Wrzesień", "Październik", "Listopad", "Grudzień" ],
			monthNamesShort: [ "Sty", "Lut", "Mar", "Kwi", "Maj", "Cze", "Lip", "Sie", "Wrz", "Paź", "Lis", "Gru" ],
			nextText: "Następny",
			prevText: "Poprzedni",
			dayNames: ["Niedziela", "Poniedziałek", "Wtorek", "Środa", "Czwartek", "Piątek", "Sobota" ],
			dayNamesMin: [ "Nd", "Pn", "Wt", "Śr", "Cz", "Pt", "So"],
			dayNamesShort: ["Nie", "Pon", "Wto", "Śro", "Czw", "Pią", "Sob" ],
			firstDay: 1,
			gotoCurrent: true,
			showOtherMonths: true,
			selectOtherMonths: true,
			beforeShowDay: function (date) {
				for (let holiday in holidays) {
					switch (holidays[holiday].type) {
						case "date" :
							if (moment(date).format("DD.MM") == holidays[holiday].options.date) {
								return [true, "ui-datepicker-holiday", holidays[holiday].name];
							}
						break;
						
						case "easter-dependent" :
							if (moment.easter(moment(date).year()).add({day:holidays[holiday].options.days_offset}).isSame(moment(date))) {
								return [true, "ui-datepicker-holiday", holidays[holiday].name];
							}
						break;
						
						case "day-of-week" :
							if (moment(date).day() == holidays[holiday].options.day) {
								return [true, "ui-datepicker-holiday", holidays[holiday].name];
							}
						break;
					}
				}
				return [true];
			}		
		});
		//TODO: Warning: it can be a race condition, when someone show callendar before AJAX response will be availabe.
		//      Hot-fix: before send AJAX request data('plannedList') set to <i class="fas fa-spinner fa-pulse"></i>
		//      After response set data('plannedList') to proper data and set in case calendar already shown $('#planned-list').html($('#recipeDetailsModal').html() to the same data);
		$('#planned-list').html($('#recipeDetailsModal').data('plannedList'));
			
		$("#recipes-datepicker").on("change", function() {
			//
		});
	});
	
	$('.modal').on('shown.bs.modal', function (e) {
		$("body").addClass("modal-open")
	});
	
	//AddCalendar
	$(document).on("planner.item.add", "#plannerMain", function (evt, response, item) {
		switch (response) {
			case "added":
				$('#planned-list .overflow-auto').append(item.date+"<br>");
				$('#recipeDetailsModal').data('plannedList', $('#recipeDetailsModal').data('plannedList').replace('</div>', item.date+"<br></div>"));
				break;
		}
	});
	
	//Toast
	$("#plannerMain").on("planner.item.add", function (evt, response, item) {
		switch (response) {
			case "added":
				label = "Dodano";
				msg = "Ddano potrawę do planera dnia: " + item.date;
				break;
			case "edited":
				label = "Zmieniono";
				msg = "Zmodyfikowano wpis dotyczący potrawy";
				break;
			default:
				label = "Błąd!";
				msg = "Wystąpił błąd podczas dodawania potrawy do planera: " + jsonResponse["status"];
				break;
		}
		$("#recipes-toast-added .toast-header").html(label);
		$("#recipes-toast-added .toast-body").html(msg);
		$("#recipes-toast-added").toast("show");
	});

  </script>
</body>

</html>
