let list = new ToDoFactory("#listsContainer");
	//list.newList( "id8362891",  [ { value:"10x Jajka", checked:false } , { value:"3l Mleko", checked:false } , { value:"2kg ziemniaków", checked:false } , { value:"300g Ser biały", checked:false } , { value:"Koperek", checked:false } , { value:"Szczypiorek", checked:false } , { value:"Sól", checked:false } , { value:"Pieprz", checked:false } , { value:"1 Cebula", checked:false } ] );
	//list.newList( "id4685132",  [ { value:"Chleb", checked:false } , { value:"Mąka [3kg]", checked:false } , { value:"Worki na śm. 30l", checked:false } ] );
	//list.newList( "id2586654",  [ { value:"[5x]&#9;Cebula", checked:false } , { value:"[2l]&#9;Mleko", checked:false } , { value:"[5kg]&#9;Makaron - Spaghetti super długie jak ten tekst do testowania tego elementu listy, który jest za krótki, żeby pomieścieć coś takiego.", checked:true } ] );
	
	$('#todoListAddEditItemModal').on('show.bs.modal', function (event) {
	  var button = $(event.relatedTarget); // Button that triggered the modal
	  var windowType = $("#NewEditItemSubmit").data("currentAction");
	  var modal = $(this)
	  if (windowType == "new") {
		modal.find('.portfolio-modal-title').text('Dodaj do listy');
		modal.find('#recipient-name').val("");
	  } else {
		modal.find('.portfolio-modal-title').text('Edytuj pozycję');
	  }
	});
	
	$('#todoListAddEditItemModal').on('shown.bs.modal', function (event) {
	  $('#recipient-name').trigger('focus');
	});
	
	$('#NewEditItemSubmit').on('click', function () {
		let newText = $(this).parent().find("input").val();

		if ($("#NewEditItemSubmit").data("currentAction") == "edit") {
			list.editItem($('#NewEditItemSubmit').data("targetObject"),newText);
			
		} else {
			list.addItem($('#NewEditItemSubmit').data("targetObject"), newText);
		}
	});
	
	$("#listsContainer").on("todo.item.add", function(evt, object) {
		$("#NewEditItemSubmit").data("targetObject", object);
		$("#NewEditItemSubmit").data("currentAction", "new");
		$('#recipient-name').val("");
		$('#todoListAddEditItemModal').modal("show");
	});
	
	$("#listsContainer").on("todo.item.edit", function(evt, object, oldValue) {
		$("#NewEditItemSubmit").data("targetObject", object);
		$("#NewEditItemSubmit").data("currentAction", "edit");
		$('#recipient-name').val(oldValue);
		$('#todoListAddEditItemModal').modal("show");
	});
	
	$("#DeleteListBtnConfirm").on("click", function() {
		list.removeList($('#DeleteListBtnConfirm').data("targetId"));
	});
	
	$("#listsContainer").on("todo.list.remove", function(evt, id) {
		$("#DeleteListBtnConfirm").data("targetId", id);
		$("#DeleteListText").html("Usuwasz listę: "+id);
		$('#todoDeleteListModal').modal("show");
	});
	
	$("#AddEmptyListBtn").on("click", function() {
		list.newList( "id"+(Math.floor(Math.random() * (89999999)) + 10000000),  [  ] );
	});
	
	$("#AddFilledListBtn").on("click", function() {
		list.newList( "id"+(Math.floor(Math.random() * (89999999)) + 10000000),  [ { value:"10x	Jajka", checked:false } , { value:"3l	Mleko", checked:false } , { value:"2kg	ziemniaków", checked:false } , { value:"300g	Ser biały", checked:false } , { value:"	Koperek", checked:false } , { value:"	Szczypiorek", checked:false } , { value:"	Sól", checked:false } , { value:"	Pieprz", checked:false } , { value:"1	Cebula", checked:false } ] );
	});
	
	//Recipe details handling
	
	$("#file-input").on("change", function() {
		console.log($(this).val());
	});
	
	$('#recipeDetailsModal').on('show.bs.modal', function (event) {
		var button = $(event.relatedTarget); // Button that triggered the modal
		//TODO: switch to loading mode
		let objId = button.attr("data-recipe-id");
		
		$("#recipeDetailsModal").attr("data-recipe-id", objId);

		plannerObj.sendIdFilteredListRequest(objId, function(jsonData) {
			let output = "";
			if (jsonData && jsonData.length) {
				output+="<b>Zaplanowałeś tą potrawę na:</b><div class=\"overflow-auto callendar-notes\">"
				for (let i in jsonData) {
					output+=jsonData[i]["date"]+"<br>";
				}
				output+="</div>";
			} else {
				output+="<b>Nie masz jeszcze w planaie tej potrawy</b>";
			}
			$('#recipeDetailsModal').data('plannedList', output);
		});
		
		$("#recipeDetailsModal .portfolio-img").prop("src", "img/portfolio/cake.png");
		$("#recipeDetailsModal .portfolio-modal-title").html('<i class="fas fa-spinner fa-pulse"></i>');
		$("#recipeDetailsModal .portfolio-ingr-list").html('<li class="list-group-item p-1"><i class="fas fa-spinner fa-pulse"></i></li>');
		$("#recipeDetailsModal .portfolio-descr").html('<i class="fas fa-spinner fa-pulse"></i>');
		$.ajax({
			url : "../backend/recipes.php?details=1&id="+objId,
			dataType : 'json',
			context : this,
			complete : function (data) {
				let objArr = data.responseJSON;

				for (let obj in objArr[0]) {
					switch (obj) {
						case "name":
							$("#recipeDetailsModal .portfolio-modal-title").html(objArr[0][obj]);
						break;
						
						case "image":
							$("#recipeDetailsModal .portfolio-img").prop("src", objArr[0][obj]);
						break;
						
						case "description":
							$("#recipeDetailsModal .portfolio-descr").html(objArr[0][obj]);
						break;
						
						case "ingredients":
							$("#recipeDetailsModal .portfolio-ingr-list").html("");
							for (let ingr in objArr[0][obj]) {
								let ingrText = isNaN(objArr[0][obj][ingr].qty) ? objArr[0][obj][ingr].name : objArr[0][obj][ingr].qty+objArr[0][obj][ingr].unit+" "+objArr[0][obj][ingr].name;
								$("#recipeDetailsModal .portfolio-ingr-list").append($("<li></li>").addClass("list-group-item p-1").html(ingrText));
							}
						break;
					}
				}
				//console.dir(objArr);
			}
		});
	});
let recipesList = new RecipesMgr("#recipesList");

let plannerObj = new PlannerMgr( { 
	mainContainer : "#plannerMain",
	dayDates : [".planner_su_date", ".planner_mo_date", ".planner_tu_date", ".planner_we_date", ".planner_th_date", ".planner_fr_date", ".planner_sa_date"],
	daysContainers: [".planner_su", ".planner_mo", ".planner_tu", ".planner_we", ".planner_th", ".planner_fr", ".planner_sa"],
	recipeTargetModal : "#recipeDetailsModal",
	startDate : moment()
});