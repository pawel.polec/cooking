"use strict";

//
// generate all available recipes
//


//      <div class="row">
//        <div class='col-md-6 col-lg-4 recipe-item'>
//          <div class="portfolio-item mx-auto" data-toggle="modal" data-target="#recipeDetailsModal" data-recipe-id="1">
//            <div class='img-dish'>
//              <div class='bottom p-2 m-1 font-weight-bold text-white'>Shoarma drobiowa z sosem czosnkowym</div>
//            </div>
//            <div class="portfolio-item-caption d-flex align-items-center justify-content-center h-100 w-100">
//              <div class="portfolio-item-caption-content text-center text-white">
//                <i class="fas fa-plus fa-3x"></i>
//              </div>
//            </div>
//            <img class="img-fluid" src="image/base64;ZZ/sdaZ?a//Z?//a/z?zz==" alt="">
//            <div class="portfolio-item-tags">
//              Kurcak ziemniaki i inne tagi po których ma szukać (all tags like name and ingredients
//            </div>
//          </div>
//        </div>
//[...]
//      </div>

/**
  * Behaviour:
  * Load only first 20, next load on scroll?
  */



  
class RecipesMgr {
	constructor(recipeDivSelector) {
		this.DOMRow = $("<div></div>").addClass("row");
		$(recipeDivSelector).append(this.DOMRow);
		
		this.initiateObj();
		this.loadAllRecipesInit();
	}
	
	loadAllRecipesResponse (data) {
            // 'this' will be what you passed as context 
			let objArr = data.responseJSON;
			
			this.DOMRow.append(this.generateItem({id:"null",thumbnail:"img/recipes/add-recipe.png",tags:"",name:"Dodaj przepis"}, "newRecipe"));
			
			for (let obj in objArr) {
				this.DOMRow.append(this.generateItem(objArr[obj]));
				this.thumnailsToLoad.push(objArr[obj].id);
			}
			
			this.state = "basic_loaded"
			this.checkThumbnails();
	}
	
	checkThumbnails() {
		if (this.thumnailsToLoad.length) {
			this.loadThumbnailRequest(this.thumnailsToLoad.shift());
		} else {
			this.state = "fully_loaded";
		}
	}
	
	loadAllRecipesInit() {
		//$.getJSON(, {obj: this}, function (result) {
		$.ajax({
			url : "../backend/recipes.php",
			dataType : 'json',
			context : this,
			complete : this.loadAllRecipesResponse
		});
		this.state = "loading"
	}
	
	loadThumbnailResponse (data) {
            // 'this' will be what you passed as context 
			let objArr = data.responseJSON;
			
			for (let obj in objArr) {
				$("#thumb_"+objArr[obj].id).attr("src", objArr[obj].thumbnail);
			}
			
			this.state = "thmb_loaded";
			this.checkThumbnails();
	}
	
	loadThumbnailRequest(id) {
		$.ajax({
			url : "../backend/recipes.php?thumbnail=1&id="+id,
			dataType : 'json',
			context : this,
			complete : this.loadThumbnailResponse
		});
		this.state = "loading_thmb";
	}
	
	initiateObj() {
		this.list = [];
		this.thumnailsToLoad = [];
		this.state = "init";
	}
	
	/**
	  * generateItem(itemData)
	  * itemData = {name: recipe title,
	  *             img: recipe thumbnail,
	  *             id: DB recipe ID,
	  *             tags: tags used to search}
	  */
	generateItem(itemData, type="normal") {
		
		let modal = "#recipeDetailsModal";
		let img = (itemData.thumbnail) ? itemData.thumbnail : "img/portfolio/cake.png";
		
		switch (type) {
			case "newRecipe":
				modal = "#addNewRecipe";
			break;
		}
		
		let DOMItem =  $("<div></div>").addClass("col-md-6 col-lg-4 recipe-item");
				
		let mainItem = $("<div></div>").addClass("portfolio-item mx-auto bg-light").attr("data-toggle","modal").attr("data-target", modal);
		mainItem.attr("data-recipe-id", itemData.id).appendTo(DOMItem);
		
		$("<div></div>").addClass("aspect-ratio-keeper").appendTo(mainItem);
		let itemWithAR = $("<div></div>").addClass("item-with-a-ratio").appendTo(mainItem);
		
		let titleDiv = $("<div></div>").addClass("img-dish").appendTo(itemWithAR);
		$("<div></div>").addClass("bottom p-2 m-1 font-weight-bold text-white").html(itemData.name).appendTo(titleDiv);
		
		let captionDiv = $("<div></div>").addClass("portfolio-item-caption d-flex align-items-center justify-content-center h-100 w-100").appendTo(itemWithAR);
		
		let captionContentDiv = $("<div></div>").addClass("portfolio-item-caption-content text-center text-white").appendTo(captionDiv);
		
		$("<i></i>").addClass("fas fa-plus fa-3x").appendTo(captionContentDiv);
		
		$("<img>").addClass("img-fluid").attr("src", img).attr("id", "thumb_"+itemData.id).appendTo(itemWithAR);
		
		$("<div></div>").addClass("portfolio-item-tags").html(itemData.tags).appendTo(mainItem);

		return DOMItem;
	}
}

