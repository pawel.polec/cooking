"use strict";


// <div class="bd-planner bd-planner-dinner bg-light" data-recipe-id="1" data-toggle="modal" data-target="#recipeDetailsModal">
//   <h4></h4>
//   <p>Shorma</p>
// </div>
  
class PlannerMgr {
	constructor(options) {
		//TODO: Error handling during initialisation.
		//this.dataTarget = options.dataTarget;
		this.dayDates = options.dayDates;
		this.daysContainers = options.daysContainers;
		this.recipeTargetModal = options.recipeTargetModal;
		this.requestedDate = options.startDate;
		this.mainContainer = options.mainContainer;
		
		this.initiateObj();
		this.loadAllRecipesInit();
	}

	onAddedResponse (response, item) {
			switch (response) {
				case "added":
					let current = moment(item.date, "DD.MM.YYYY");
					if (current.isBetween(this.startDate, this.endDate, null, '[]')) {
						$(this.daysContainers[current.day()]).append(this.generateItem(item));
					}
					break;
			}
	}

	loadAllRecipesResponse (data) {
			let objArr = data.responseJSON;
			this.visibleData = objArr;
			
			for (let date = moment(this.startDate); date.isSameOrBefore(this.endDate); date.add({day:1})) {
				$(this.dayDates[date.day()]).html(date.format("DD.MM"));
			}
			
			for (let recipe in objArr) {
				$(this.daysContainers[moment(objArr[recipe].date, "DD.MM.YYYY").day()]).append(this.generateItem({name:objArr[recipe].name, id:objArr[recipe].id, type:objArr[recipe].type}));
			}
			
			this.state = "basic_loaded";
	}
	
	loadAllRecipesInit() {
		$.ajax({
			url : "../backend/planner.php?action=get&s="+this.startDate.format("DD.MM.YYYY")+"&e="+this.endDate.format("DD.MM.YYYY"),
			dataType : 'json',
			context : this,
			complete : this.loadAllRecipesResponse
		});
		this.state = "loading"
	}
	
	addToPlanner(id, date, portions, resultCbf) {
		this.activeResultCbf = resultCbf;
		$.ajax({
			url : "../backend/planner.php?action=add&id="+id+"&date="+date+"&portions="+portions,
			dataType : 'json',
			context : this,
			complete : function (data) {
				this.activeResultCbf(data.responseJSON);
				this.onAddedResponse (data.responseJSON["state"].status, data.responseJSON["state"].load);
				$(this.mainContainer).trigger( "planner.item.add", [data.responseJSON["state"].status, data.responseJSON["state"].load] );
			}
		});
	}
	
	sendIdFilteredListRequest(id, resultCbf) {
		this.activeResultCbf = resultCbf;
		$.ajax({
			url : "../backend/planner.php?action=get&sd="+moment().format("DD.MM.YYYY")+"&id="+id,
			dataType : 'json',
			context : this,
			complete : function (data) {
				this.activeResultCbf(data.responseJSON);
			}
		});
	}
	
	initiateObj() {
		this.list = [];
		this.state = "init";
		
		this.startDate = moment(this.requestedDate).startOf('isoWeek');
		this.endDate   = moment(this.requestedDate).endOf('isoWeek');
	}
	
	/**
	  * generateItem(itemData)
	  * itemData = {name: recipe title,
	  *             id: DB recipe ID,
	  *             type: dish type [ dessert, dinner, breakfast...]}
	  */
	generateItem(itemData) {
		
		let modal = this.recipeTargetModal;
		let typeClass = "bd-planner-dinner"
		switch (itemData.type) {
			case "dessert":
				typeClass = "bd-planner-dessert";
				break;
			case "breakfast":
				typeClass = "bd-planner-breakfast";
				break;
		}
		
		let DOMItem =  $("<div></div>").addClass("bd-planner bg-light " + typeClass).attr("data-toggle","modal").attr("data-target", modal).attr("data-recipe-id", itemData.id);

		$("<h4></h4>").appendTo(DOMItem);
		
		$("<p></p>").html(itemData.name).appendTo(DOMItem);

		return DOMItem;
	}
}

