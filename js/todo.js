"use strict"; // Start of use strict
(function($) {
	const animTime=200;

	//side: "before" of "after"
	var animateColapse = function (row, side, targetValue, h) {
		let cssToChange = 'null';
		if (side == "before") {
			cssToChange = {marginTop: targetValue};
			row.css('marginTop',	h);
		}
		else if (side == "after") {
			cssToChange = {marginBottom:targetValue};
			row.css('marginBottom',	h);
		}
		
		if (cssToChange == 'null') throw 'Parameter "side" invalid: use before or after';
		
		row.animate(cssToChange, {duration: animTime, queue: false});
	}
	
	var animateMove = function (row, targetValue) {
		row.animate({top: targetValue}, {duration: animTime, queue: false});
	}
	
	let checkboxOnChange = function() {
		let row = $(this).parent().parent();
		let rowPreset = {
			position: 'absolute',
			width: row.outerWidth(),
			top: row.position().top,
			zIndex: 5
		}
		
		let triggerElem = row.parent().parent().parent();
		let rmIndex = row.parent().children().filter("li").index(row);
		let triggerElemId = row.parent().parent().parent().attr("id");
	 
		if ($(this).prop('checked')) {
			if (row.is(":last-child")) return; //no animation
			let W0 = {
				lastTop: row.parent().children(':last').position().top,
				itemMargin: parseInt(row.css('marginBottom')) + parseInt(row.css('marginTop')),
				h : row.outerHeight()
			}
			row.css(rowPreset);
			row.siblings().filter("li").css({zIndex: 2});
			animateMove(row, W0.lastTop);
			animateColapse(row.next(),'before',0, W0.h + W0.itemMargin);
			row.siblings(":last").animate({
				marginBottom: W0.h + W0.itemMargin
			}, animTime, function() {
				row.siblings(":last").css('marginBottom', '');
				row.parent().children(':last').after(row);
				row.css({
					position: 'relative',
					top: 0,
					width: ''
				});
				row.siblings().filter("li").css({
					marginTop: '',
					marginBottom: ''
				});
				triggerElem.trigger( "todo.item.moved", [triggerElemId, rmIndex, "bottom"] );
			});
	 
	 } else {
		if (row.is(":first-child")) return;
			let W0 = {
				firstTop: row.parent().children(':first').position().top,
				itemMarginBottom: Math.min(parseInt(row.css('marginBottom')), -1), //investigate why -1 No needed in Edge (MS - of course (-_-)	)
				h : row.outerHeight()
			}
			row.css(rowPreset);
			row.siblings(":first").css({marginTop: W0.itemMarginBottom});
			row.siblings().filter("li").css({zIndex: 2});
			animateMove(row, W0.firstTop);
			animateColapse(row.prev(), "after", W0.itemMarginBottom, W0.h + W0.itemMarginBottom);
			row.siblings(":first").animate({
				marginTop: W0.h + W0.itemMarginBottom
			}, animTime, function() {
				row.siblings(":first").css('marginTop', '');
				row.parent().children(':first').before(row);
				row.css({
				position: 'relative',
				top: 0,
				width: ''
				});
				row.siblings().filter("li").css({
				marginTop: '',
				marginBottom: ''
				});
				
				triggerElem.trigger( "todo.item.moved", [triggerElemId, rmIndex, "top"] );
			});
		}
	 }

	let removeItemOnClick = function() {
		let row = $(this).parent().parent();
		row.addClass("todo-removing-item");
		let delRow = $("<div></div>").html('<button type="button" class="btn btn-xs btn-outline-primary">Anuluj</button>');
		delRow.addClass("todo-remove-abort");
		delRow.css({
			overflow: 'hidden',
			maxHeight: row.outerHeight()-2,
			position: "relative",
			marginTop: - row.outerHeight()+2,
			width:0,
			height:row.outerHeight()-2
		});
		row.after(delRow);
		delRow.animate({width:row.outerWidth()}, {duration: animTime, queue: false});

		setTimeout(function() {
			let triggerElemId = row.parent().parent().parent().attr("id");
			let triggerElem = row.parent().parent().parent();
			let rmIndex = 0;
			let current = row.parent().children(':first');

			if (row.hasClass("todo-removing-item")){
				rmIndex	= row.parent().children().filter("li").index(row);

				row.slideUp(450);
				delRow.slideUp({duration: 500, queue: false});
				delRow.fadeTo( 500, 0, function() {
					delRow.remove();
					row.remove();
				});

				triggerElem.trigger( "todo.item.removed", [triggerElemId, rmIndex] );
				//remove from LIST/DB
			}
		}, 3000)
		//1. animate to temorarty removed (swipe to panel with cancel button)
		//2. wait 2 sec
		//3. If not Cancel remove:
		//	3.1 animate remove permanantly (Fade out "cancel item" then olapse list)
		//	3.2 save do DB
		//4. Else
		//	4.1 Anime to recover list (swipe to original item)

		//known issues:
		//Remove last item -> check not last: problem with margins and filtering of last element. Maybe slideUp would be helpfull (special dummy element would be required)
		//To long text destroys display and animation.
	}

	let removeItemAbortOnClick = function() {
		let row = $(this).parent().prev();
		row.removeClass("todo-removing-item");
		//TODO: clearTimeout!
		
		$(this).parent().fadeTo( 500, 0, function() {
			$(this).remove();
		});
	}

	let addItemOnClick = function() {
		let todoList = $(this).parents(".todo-list");
		$(this).parents(".row").parent().trigger( "todo.item.add", [todoList]);
	}

	let editItemOnClick = function() {
		let liElement = $(this).parents("li");
		$(this).parents(".row").parent().trigger( "todo.item.edit", [liElement, liElement.find("label").html()] );
	}

	let removeListOnClick = function() {
		let todoList = $(this).parents(".todo-list");
		$(this).parents(".row").parent().trigger( "todo.list.remove", [todoList.attr("id")]);
	}

	let itemRemovedEvent = function(e, a, b) {
		console.log("todo.item.removed triggered: "+ e +", "+a+", "+b);
	}

	let itemMovedEvent = function(e, a, b, c) {
		console.log("todo.item.moved triggered: "+ e +", "+a+", "+b+", "+c);
	}

	$.fn.extend({
		'refreshAllTodoListeners': function(_options) {
			$('input[type=checkbox]').off("change");
			$('.todo-remove-list').off("click");
			$(".todo-list").off("todo.item.removed", itemRemovedEvent);
			$(".todo-list").off("todo.item.moved", itemMovedEvent);
			$('.todo-remove-item').off("click");

			$('.todo-add-item').off("click");
			$('.todo-edit-item').off("click");
			$('div').off("click", ".todo-remove-abort button", removeItemAbortOnClick );

			$('input[type=checkbox]').on("change", checkboxOnChange);
			$('.todo-remove-list').on("click", removeListOnClick);
			$(".todo-list").on("todo.item.removed", itemRemovedEvent);
			$(".todo-list").on("todo.item.moved", itemMovedEvent);
			$('.todo-remove-item').on("click", removeItemOnClick);

			$('.todo-add-item').on("click", addItemOnClick);
			$('.todo-edit-item').on("click", editItemOnClick);
			$('div').on("click", ".todo-remove-abort button", removeItemAbortOnClick );
		}
	});

})(jQuery);

class ToDoFactory {
	constructor(listDivId) {
		this.listRow = $("<div></div>").addClass("row");

		$(listDivId).append(this.listRow);
		this.lists = [];
		//TODO: check if listRowId is OK and listRow is an element and move here row generation
	}

	generateElement(newId, newElement) {
		let liListItem = $("<li></li>");
		liListItem.addClass("list-group-item");

		let divCustChkbox = $("<div></div>");
		divCustChkbox.addClass("custom-control custom-checkbox");
		liListItem.append(divCustChkbox);

		//let tempId = "item-" + list.id + "-" + i;
		let inputChkbox = $("<input>").attr({
			type: 'checkbox',
			id: newId,
			checked: newElement.checked
		});
		inputChkbox.addClass("custom-control-input");
		divCustChkbox.append(inputChkbox);

		let labelChkbox = $("<label></label>").attr({for: newId});
		labelChkbox.addClass("custom-control-label noselect");
		labelChkbox.html(newElement.value);
		divCustChkbox.append(labelChkbox);

		let btnRemove = $("<button></button>");
		btnRemove.html('<i class="fas fa-eraser fa-xs"></i>');
		btnRemove.addClass("btn btn-outline-secondary float-right btn-xs todo-remove-item");
		divCustChkbox.append(btnRemove);

		let btnEdit = $("<button></button>");
		btnEdit.html('<i class="fas fa-edit fa-xs"></i>');
		btnEdit.addClass("btn btn-outline-secondary float-right btn-xs todo-edit-item");
		divCustChkbox.append(btnEdit);

		return liListItem;
	}

	generateList(list) {
	//			GENERATE STRUCTURE LIKE BELOW
	//	<div class="row">
	//	<div class="col-lg-8 mx-auto">
	//		<div class="card todo-list" id="list29861">
	//		<div class="card-header">Lista zakupów <button class="btn btn-outline-secondary btn-xs todo-add-item" title="Dodaj do listy"><i class="fas fa-plus fa-xs"></i></button></div>
	//		<div class="card-body">
	//			<ul class="list-group">
	//			<li class="list-group-item">
	//				<div class="custom-control custom-checkbox">
	//				<input type="checkbox" class="custom-control-input" value="" id="item1">
	//				<label class="custom-control-label noselect" for="item1">Jajka</label>
	//				<button class="btn btn-outline-secondary float-right btn-xs todo-remove-item"> <i class="fas fa-eraser fa-xs"></i></button>
	//				<button class="btn btn-outline-secondary float-right btn-xs todo-edit-item"> <i class="fas fa-edit fa-xs"></i></button>
	//				</div>
	//			</li>
	//			<li class="list-group-item">
	//				<div class="custom-control custom-checkbox">
	//				<input type="checkbox" class="custom-control-input" value="" id="item2">
	//				<label class="custom-control-label noselect" for="item2">Mąka</label>
	//				<button class="btn btn-outline-secondary float-right btn-xs todo-remove-item"><i class="fas fa-eraser fa-xs"></i></button>
	//				<button class="btn btn-outline-secondary float-right btn-xs todo-edit-item"> <i class="fas fa-edit fa-xs"></i></button>
	//				</div>
	//			</li>
	//			</ul>
	//		</div>
	//		<div class="card-footer">
	//			<button class="btn btn-outline-secondary">Usuń listę <i class="fas fa-times"></i></button>
	//		</div>
	//		</div>
	//	</div>
	//	</div>
		let divColsUpper = $("<div></div>");
		divColsUpper.addClass("col-lg-8 mx-auto todo-container");

		let divCard = $("<div></div>");
		divCard.attr("id", list.id);
		divCard.addClass("card todo-list");

		let divCardHeader = $("<div></div>");
		divCardHeader.html('Lista zakupów <button class="btn btn-outline-secondary btn-xs todo-add-item" title="Dodaj do listy"><i class="fas fa-plus fa-xs"></i></button>');
		divCardHeader.addClass("card-header");
		divCard.append(divCardHeader);

		let divCardBody = $("<div></div>");
		divCardBody.addClass("card-body");
		divCard.append(divCardBody);

		let divCardFooter = $("<div></div>");
		divCardFooter.html('<button class="btn btn-outline-secondary todo-remove-list">Usuń listę <i class="fas fa-times"></i></button>');
		divCardFooter.addClass("card-footer");
		divCard.append(divCardFooter);

		let ulList = $("<ul></ul>");
		ulList.addClass("list-group");
		divCardBody.append(ulList);

		for (let i in list.elems) {
			let tempId = "item-" + list.id + "-" + i;
			ulList.append(this.generateElement(tempId, list.elems[i]));
		}

		list.highestIdIdx = list.elems.length-1;

		divColsUpper.append(divCard);

		this.listRow.append(divColsUpper);

		$.fn.refreshAllTodoListeners();
	}//<div class="card-header">Lista zakupów <button class="btn btn-outline-secondary btn-xs todo-add-item" title="Dodaj do listy"><i class="fas fa-plus fa-xs"></i></button></div>

	privRemoveItemHandler(evt, id, index) {
		let obj = evt.data.obj;
		for (let i in obj.lists) {
			if (obj.lists[i].id == id) {
				obj.lists[i].elems.splice(index, 1);
			}
		}
	}

	privMovedItemHandler(evt, id, mvFrom, mvTo) {

		let moveToIdx = (mvTo == "top") ? 0 : mvTo;

		let obj = evt.data.obj;
		for (let i in obj.lists) {
			if (obj.lists[i].id == id) {
				if (mvTo == "top") moveToIdx = 0
				else if (mvTo == "bottom") moveToIdx = obj.lists[i].elems.length-1;
				let movingItem = obj.lists[i].elems.splice(mvFrom, 1)[0];
				obj.lists[i].elems.splice(moveToIdx, 0, movingItem);
				break;
			}
		}
	}

	newList(id, elems=[]) {
		this.lists.push({
			id: id,
			elems: elems
		});
		//TODO: validate elems

		this.generateList(this.lists[this.lists.length-1]);

		$(".todo-list").off("todo.item.removed",this.privRemoveItemHandler);
		$(".todo-list").on("todo.item.removed", {obj: this}, this.privRemoveItemHandler);

		$(".todo-list").off("todo.item.moved",this.privMovedItemHandler);
		$(".todo-list").on("todo.item.moved", {obj: this}, this.privMovedItemHandler);
	}

	editItem(obj, newValue) {
		let id = $(obj).parents(".todo-list").attr("id");
		let index = $(obj).parent().children().filter("li").index(obj);

		for (let i in this.lists) {
			if (this.lists[i].id == id) {
				this.lists[i].elems[index].value = newValue;
				break;
			}
		}
		$(obj).find("label").html(newValue);
		
	}

	addItem(obj, newValue) {
		let listId = obj.attr("id");
		for (let i in this.lists) {
			if (this.lists[i].id == listId) {
				this.lists[i].elems.splice(0, 0, {value: newValue, checked:false});
				
				let tempId = "item-" + list.id + "-" + this.lists[i].highestIdIdx++;
				let newRow = this.generateElement(tempId, this.lists[i].elems[0])
				this.listRow.find("#"+listId).find("ul").prepend(newRow);
				
				$.fn.refreshAllTodoListeners();
				break;
			}
		}
	}

	removeList(listId) {
		for (let i in this.lists) {
			if (this.lists[i].id == listId) {
				this.lists.splice(i, 1);

				let list = $("#"+listId).parent();
				list.fadeTo( 1500, 0 ).slideUp(500, function() {
					list.remove();
				});
				break;
			}
		}
	}
}
